#!/usr/bin/env node --harmony

const program = require('commander');
const pkg = require('./package');

['init', 'push', 'work'].forEach(command => {
  require(`./lib/commands/${command}`)(program);
});

program
  .version(pkg['version'])
  .parse(process.argv);
