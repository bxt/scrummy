scrummy
=======

[![build status](https://gitlab.com/bxt/scrummy/badges/master/build.svg)](https://gitlab.com/bxt/scrummy/commits/master)
[![NPM Version](http://img.shields.io/npm/v/scrummy.svg?style=flat)](https://www.npmjs.org/package/scrummy)
[![NPM Downloads](https://img.shields.io/npm/dm/scrummy.svg?style=flat)](https://www.npmjs.org/package/scrummy)
[![coverage report](https://gitlab.com/bxt/scrummy/badges/master/coverage.svg)](https://bxt.gitlab.io/scrummy/coverage/lcov-report/)

An opinionated command line tool for managing Trello, GitLab and Harvest for Scrum.

> [«Yummy, Yummy, Scrummy. I got love in my tummy!»](https://www.youtube.com/watch?v=-uo9tMoew6o)

Usage
-----

Install using:

    npm install -g scrummy

**Init**:

    scrummy init

- [x] Asks for your Trello credentials
- [x] Asks which board to use for the project
- [x] Asks how to assign the lists (backlog/development/review/done)
- [ ] Asks for your Harvest credentials
- [x] Asks for your GitLab credentials
- [x] Asks for your GitLab review peers

**Work**:

    scrummy work

- [x] Asks which trello card you want to work on

If you choose a development card:

- [x] Assigns you to the trello card, if not assigned yet
- [ ] Starts a harvest timer on the card
- [x] Moves the trello card from backlog into development, if not there yet
- [x] Checks if on development branch, or asks if to switch to it, if on:
  - [x] Pulls
  - [x] Runs something like `git-deletebranches.sh`
- [x] Creates and checks out a branch with the name of the card, if branch does not yet exist
- [ ] Pulls the branch
- [x] Displays the info from the card

Or if you choose a review card:

- [ ] Starts a harvest timer on the card
- [ ] Asks if you want to check out the code, does so if necessary
- [ ] Opens the merge request on GitLab

**Push**:

    scrummy push

If you're working on a development card:

- [x] Pushes the current branch
- [x] Asks to tick off some card todos
- [x] When no MR: Creates a MR on GitHub
- [x] Puts a trello card's contents and a link to it into the MR
- [x] Puts the link to the MR into a card attachment if not there yet
- [x] Asks if finished
  - [x] When all todos are done
  - [ ] And the MR has no conflicts
  - [x] Moves the trello card into review list
  - [ ] Removes WIP from the MR
  - [ ] Stops any harvest timer on the card
  - [ ] ¿Waits for the build to finish
  - [x] Assigns someone else on the MR

Developing
----------

To use your local copy of the package in the command line run:

    npm link

To **install** all the required packages do:

    npm install

To run the **tests** ([assertions info](http://chaijs.com/api/bdd/)) use:

    npm test
    # or
    npm -s test -- -w

To **lint** the JS run:

    $(npm bin)/eslint lib/** test/**

Use `--fix` to fix some stuff automatically.


See also:

* [Scripting with node](https://developer.atlassian.com/blog/2015/11/scripting-with-node/)
* [Trello Developers](https://developers.trello.com/)
