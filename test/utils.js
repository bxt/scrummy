const { expect } = require('chai');
const { brace, findIndices } = require('../lib/utils');

describe('utils', function () {
  describe('#brace(string)', function () {
    it('should return the input string in braces', function () {
      expect(brace('a')).to.equal('(a)');
      expect(brace('foo1234')).to.equal('(foo1234)');
    });
  });

  describe('#findIndices(array, predicate)', function () {
    it('should return the indices of the elements matching the predicate', function () {
      expect(findIndices([1, 2, 3, 4, 5], x => x >= 3)).to.deep.equal([2, 3, 4]);
      expect(findIndices([5, 0, 1, 9, 2, 2, 7, 1, 11], x => x >= 3)).to.deep.equal([0, 3, 6, 8]);
    });
  });
});
