const prompt = require('co-prompt');
const exec = require('co-exec');
const chalk = require('chalk');

function brace(string) {
  return `(${string})`;
}

function* promptWithDefault(label, defaultValue) {
  return (yield prompt(label + ' ' + (defaultValue === undefined ? '' : brace(defaultValue) + ' '))) || defaultValue;
}

function* promptSelect(label, possibleValues, defaultValue, defaultValueFinder) {
  const [min, max] = [0, possibleValues.length];
  var defaultIndex = possibleValues.findIndex(defaultValueFinder || (v => v.id === defaultValue));
  if (defaultIndex === -1) defaultIndex = 0;
  let valueIndex = yield promptWithDefault(label, defaultIndex);
  while (valueIndex < min || valueIndex >= max) {
    valueIndex = yield promptWithDefault(`Please enter a number between ${min} and ${max}`, defaultIndex);
  }
  return possibleValues[valueIndex];
}

function findIndices(array, predicate) {
  return array.reduce((acc, v, i) => predicate(v) ? [...acc, i] : acc, []);
}

function* promptMultiSelect(label, possibleValues, defaultValues = []) {
  const [min, max] = [0, possibleValues.length];
  const defaultIndices = findIndices(possibleValues, v => defaultValues.indexOf(v) !== -1);
  const defaultIndicesString = defaultIndices.join(', ');
  let valid = false;
  let indices = false;
  while (!valid) {
    const indicesString = yield promptWithDefault(label, defaultIndicesString);
    indices = indicesString.split(',').map(x => x*1);
    valid = true;
    indices.forEach(valueIndex => {
      if (valueIndex < min || valueIndex >= max) {
        valid = false;
        console.log(chalk.red(`Please enter a number between ${min} and ${max} instead ${valueIndex}`));
      }
    });
  }
  return indices.map(valueIndex => possibleValues[valueIndex]);
}

function handleActionPromise(promise) {
  promise.then(() => {
    process.exit(0);
  }, err => {
    console.log(chalk.red(err.message));
    console.log(err);
    process.exit(1);
  });
}

function* gitLabURLs() {
  const sedResult = yield exec('git remote show -n origin | sed -n \'/^  Fetch URL: git@\\(.*\\):\\(.*\\)\\.git$/ s//\\1|\\2/p\'');
  const [domain, pathWithNamespace] = sedResult.trim().split('|');
  const [namespace, path] = pathWithNamespace.split('/');
  const webUrl = `https://${domain}/${pathWithNamespace}`;
  const gitLabUrl = `https://${domain}`;
  return { domain, path, namespace, pathWithNamespace, webUrl, gitLabUrl };
};

module.exports = { brace, promptWithDefault, promptSelect, findIndices, promptMultiSelect, handleActionPromise, gitLabURLs };
