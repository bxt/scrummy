const Gitlab = require('gitlab');
const exec = require('co-exec');

const config = require('./config');
const { gitLabURLs } = require('./utils');

const MR_TARGET_BRANCH = 'development';

class MyGitLab {
  constructor(urls, token) {
    this.urls = urls;
    this.gl = Gitlab({ url: urls.gitLabUrl, token });
  }
  * getProject() {
    const projects = yield (c) => this.gl.projects.all({search: this.urls.path}, res => c(null, res));
    const project = projects.find(p => p.path_with_namespace === this.urls.pathWithNamespace);
    if (!project) throw new Error(`Could not find project with path "${this.urls.pathWithNamespace}"`);
    return project;
  }
  * getMergeRequest(project) {
    const branchName = (yield exec('git rev-parse --abbrev-ref HEAD')).trim();
    const mergeRequests = yield (c) => this.gl.projects.merge_requests.list(project.id, { state: 'opened' }, res => c(null, res));
    const mergeRequest = mergeRequests.find(mr => mr.source_branch === branchName);
    return mergeRequest;
  }
  * getCurrentUser() {
    const user = yield (c) => this.gl.users.current(res => c(null, res));
    return user;
  }
  * addMergeRequest(project, title) {
    const assignee = yield this.getCurrentUser();
    const params = {
      source_branch: (yield exec('git rev-parse --abbrev-ref HEAD')).trim(),
      target_branch: MR_TARGET_BRANCH,
      title,
      assignee_id: assignee.id,
      remove_source_branch: true,
    };
    const mergeRequest = yield (c) => this.gl.post(`projects/${project.id}/merge_requests`, params, res => c(null, res));
    return mergeRequest;
  }
  * updateMergeRequest(mergeRequest, params) {
    mergeRequest = yield (c) => this.gl.projects.merge_requests.update(mergeRequest.project_id, mergeRequest.id, params, res => c(null, res));
    return mergeRequest;
  }
  * getAllUsers(project) {
    const users = yield (c) => this.gl.users.all(res => c(null, res));
    return users;
  }
}

module.exports = function* ({token = config.get('gitLabToken')} = {}) {
  const urls = yield gitLabURLs();
  return new MyGitLab(urls, token);
};
