const Trello = require('node-trello');

const config = require('./config');
const KEY = require('./key');

const CHECK_LIST_TITLE = 'acceptance criteria';

const memberAssignedToCard = member => card => card.idMembers.indexOf(member.id) !== -1;

class MyTrello extends Trello {

  * getMe() {
    return (yield this.runAsync('get', '/1/members/me', {
      boards: 'all',
      board_fields: 'name,starred,idOrganization',
      organizations: 'all',
      organization_fields: 'displayName',
    }));
  }

  * getBoardLists(boardId) {
    return (yield this.runAsync('get', `/1/boards/${boardId}/lists`));
  }

  * getCard(shortLink) {
    return (yield this.runAsync('get', `/1/cards/${shortLink}?fields=name,shortLink,url,desc`));
  }

  * getListCards(listId) {
    return (yield this.runAsync('get', `/1/lists/${listId}/cards`));
  }

  * getListCardsOfMember(listId, member) {
    const cards = yield this.getListCards(listId);
    return cards.filter(memberAssignedToCard(member));
  }

  * getCardChecklist(card) {
    const checklists = yield this.runAsync('get', `/1/cards/${card.id}/checklists`);
    const checklist = checklists.find(list => list.name.toLowerCase().indexOf(CHECK_LIST_TITLE) !== -1);
    if (checklist) checklist.checkItems.sort((a, b) => a.pos - b.pos);
    return checklist;
  }

  * updateCard(card, params) {
    return (yield this.runAsync('put', `/1/cards/${card.id}`, params));
  }

  * completeCardCheckItem(card, item) {
    return (yield this.runAsync('put', `/1/cards/${card.id}/checkItem/${item.id}`, { state: 'complete' }));
  }

  * ensureCardAttachment(card, name, url) {
    const attachments = yield this.runAsync('get', `/1/cards/${card.id}/attachments`);
    if (!attachments.find(attachment => attachment.name === name)) {
      yield this.runAsync('post', `/1/cards/${card.id}/attachments`, { name, url });
      return true;
    } else {
      // TODO: refresh if link changed?
      return false;
    }
  }

  * addCardMember(card, member) {
    return (yield this.runAsync('post', `/1/cards/${card.id}/idMembers`, { value: member.id }));
  }

  * ensureCardMember(card, member) {
    if (!memberAssignedToCard(member)(card)) {
      yield this.addCardMember(card, member);
      return true;
    } else {
      return false;
    }
  }

  * runAsync(method, path, params) {
    return (yield (c) => this[method](path, params, c));
  }
}

module.exports = ({key = KEY, token = config.get('trelloToken')} = {}) => new MyTrello(key, token);
