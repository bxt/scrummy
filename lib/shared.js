const chalk = require('chalk');

const config = require('./config');

module.exports.moveCard = function* (t, card, type) {
  const idList = config.get(`${type}ListId`);
  if (card.idList !== idList) {
    console.log('Moving the card to %s...', type);
    yield t.updateCard(card, { idList });
  }
};

module.exports.cardBranch = function (card) {
  const cardSlug = card.url.match(/^https:\/\/trello\.com\/c\/[^/]+\/\d+-(\d+-)?(.*)$/)[2];
  const branchName = `trello/${cardSlug}-${card.shortLink}`;
  return branchName;
};

module.exports.displayCheckItem = function (checkItem, prefix='  ') {
  const complete = checkItem.state !== 'incomplete';
  const string = `${prefix}[${complete ? '✓' : ' '}] ${checkItem.name}`;
  console.log(complete ? chalk.dim(string) : string);
};
