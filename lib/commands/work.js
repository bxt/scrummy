const co = require('co');
const exec = require('co-exec');
const chalk = require('chalk');
const { confirm } = require('co-prompt');

const config = require('../config');
const MyTrello = require('../MyTrello');
const { brace, promptSelect, handleActionPromise } = require('../utils');
const { moveCard, cardBranch, displayCheckItem } = require('../shared');

function displayCardTitle(card, i) {
  const checkItemsString = card.badges.checkItems ? brace(card.badges.checkItemsChecked + '/' + card.badges.checkItems) : '';
  const commentsString = card.badges.comments + '💬';
  console.log('  (%d) %s %s %s', i, card.name, checkItemsString, commentsString);
}

function* askTelloCard(t, me) {
  let developmentCards = yield t.getListCardsOfMember(config.get('developmentListId'), me);
  const backlogCards = yield t.getListCards(config.get('backlogListId'));

  console.log('Already worked on:');
  developmentCards.forEach(displayCardTitle);
  if (developmentCards.length === 0) {
    console.log(chalk.grey('  – none –'));
  }

  console.log('Waiting to be worked on:');
  backlogCards.forEach((card, i) => {
    displayCardTitle(card, i + developmentCards.length);
  });
  if (backlogCards.length === 0) {
    console.log(chalk.grey('  – none –'));
  }

  const card = yield promptSelect('Which card do you want to work on?', [...developmentCards, ...backlogCards]);
  console.log('Working on card %s with shortLink %s', card.name, card.shortLink);
  return card;
}

function* assignTrelloCard(t, member, card) {
  if (yield t.ensureCardMember(card, member)) {
    console.log('Assigned you to the card.');
  }
}

function* startHarvestTimerOnCard(card) {
  // TODO
}

function* checkoutCardBranch(card) {
  const branchName = cardBranch(card);

  try {
    yield exec(`git rev-parse --verify ${branchName}`);
  } catch (e) { // branch does not exist
    yield prepareGitDevelopmentBranch();
    yield createBranch(branchName);
  }

  yield exec(`git checkout ${branchName}`);
}

function* prepareGitDevelopmentBranch() {
  const currentBranch = (yield exec('git rev-parse --abbrev-ref HEAD')).trim();
  const wasOnDevelopment = currentBranch === 'development';
  var change = false;
  if (!wasOnDevelopment) {
    change = yield confirm(`Currently on ${chalk.bold(currentBranch)}. Do you want to switch to development branch before branching?`);
    if (change) {
      console.log('Switching to development branch...');
      yield exec('git checkout development');
    }
  }
  if (wasOnDevelopment || change) {
    console.log('Fetching...');
    yield exec('git fetch -p');
    console.log('Merging...');
    yield exec('git merge FETCH_HEAD');
  }
}

function* createBranch(branchName) {
  console.log(`Checkout new branch ${branchName}`);
  yield exec(`git checkout -b ${branchName}`);
}

function* displayChecklist(t, card) {
  const checklist = yield t.getCardChecklist(card);
  if (checklist) {
    console.log(checklist.name);
    checklist.checkItems.forEach(item => {
      displayCheckItem(item);
    });
  }
}

function* displayCard(t, card) {
  console.log('-----');
  console.log(chalk.bold(card.name));
  console.log(card.desc);
  console.log();
  yield displayChecklist(t, card);
  console.log();
  console.log(chalk.underline(card.url));
}

module.exports = program => program
  .command('work')
  .description('Start working on something')
  .action(function (env, options) {
    handleActionPromise(co(function *() {
      const t = MyTrello();
      const me = yield t.getMe();
      const card = yield askTelloCard(t, me);
      yield assignTrelloCard(t, me, card);
      yield startHarvestTimerOnCard(card);
      yield moveCard(t, card, 'development');
      yield checkoutCardBranch(card);
      yield displayCard(t, card);
    }));
  });
