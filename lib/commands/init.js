const co = require('co');
const chalk = require('chalk');

const MyTrello = require('../MyTrello');
const MyGitLab = require('../MyGitLab');
const config = require('../config');
const KEY = require('../key');
const { brace, promptWithDefault, promptSelect, promptMultiSelect, handleActionPromise, gitLabURLs } = require('../utils');

function trelloAuthUrl(key) {
  return `https://trello.com/1/connect?key=${key}&name=scrummy&response_type=token&scope=read,write&expiration=never`;
}

function* askTelloToken() {
  var token = config.get('trelloToken', { ignoreNotFound: true });

  console.log('Please authorize scrummy to access your Trello by going to: ');
  console.log(chalk.bold(trelloAuthUrl(KEY)));
  token = yield promptWithDefault('Token:', token);

  const t = MyTrello({ token });
  const myData = yield t.getMe();
  config.set('trelloToken', token);
  console.log('Hello, %s! Thank you for using scrummy (:', myData.fullName);

  return myData;
}

function* askTrelloBoard(myData) {
  console.log('These are your boards:');
  myData.boards.forEach((board, i) => {
    const organization = myData.organizations.find((o) => o.id === board.idOrganization);
    console.log('%s (%d) %s %s', (board.starred ? '★' : ' '), i, board.name, organization ? brace(organization.displayName) : '');
  });
  const board = yield promptSelect('Which board do you want to use for this project?', myData.boards, config.get('boardId', { ignoreNotFound: true }));
  config.set('boardId', board.id);
  console.log('Using board %s with id %s', board.name, board.id);
  return board.id;
}

function* askTrelloLists(boardId) {
  const t = MyTrello();
  const lists = yield t.getBoardLists(config.get('boardId'));

  console.log('These are the lists on this board:');
  lists.forEach((list, i) => {
    console.log('  (%d) %s', i, list.name);
  });

  const types = ['backlog', 'development', 'review', 'done'];
  for (let i = 0; i < types.length; i++) {
    const type = types[i];
    const possibleDefault = lists.find(list => list.name.toLowerCase().indexOf(type) !== -1) || {};
    const configKey = `${type}ListId`;
    const list = yield promptSelect(`Which list do you want to use as ${type}?`, lists, config.get(configKey, { ignoreNotFound: true, defaultValue: possibleDefault.id }));
    config.set(configKey, list.id);
    console.log('Using list %s with id %s as %s', list.name, list.id, type);
  }
}

function gitLabAuthUrl(URLs) {
  return `${URLs.gitLabUrl}/profile/personal_access_tokens`;
}

function* askGitLabToken() {
  const URLs = yield gitLabURLs();
  var token = config.get('gitLabToken', { ignoreNotFound: true });

  console.log('Please authorize scrummy to access your GitLab by creating a token at: ');
  console.log(chalk.bold(gitLabAuthUrl(URLs)));
  token = yield promptWithDefault('Token:', token);

  const gl = yield MyGitLab({ token });
  const project = yield gl.getProject();
  config.set('gitLabToken', token);
  console.log('Using GitLab project %s with id %s', project.name_with_namespace, project.id);

  return project;
}

function gitLabUserAge(user) {
  return Math.floor((new Date() - new Date(user.created_at))/1000/60/60/24);
}

function* askGitLabPeers(project) {
  const gl = yield MyGitLab();

  const me = yield gl.getCurrentUser();
  console.log(`We know you on GitLab as %s (%s) and you're %d days in!`, me.name, me.username, gitLabUserAge(me));

  const members = yield gl.getAllUsers();
  const possibleMembers = members.filter(m => m.id !== me.id);
  possibleMembers.forEach((member, i) => {
    console.log('  (%d) %s (%s)', i, member.name, member.username);
  });
  const memberIDs = possibleMembers.map(m => m.id);
  const defaultIDs = config.get('gitLabPeers', { ignoreNotFound: true, defaultValue: memberIDs });

  const peerIDs = yield promptMultiSelect('Who do you feel worthy of reviewing your MRs?', memberIDs, defaultIDs);
  config.set('gitLabPeers', peerIDs);
}

module.exports = program => program
  .command('init')
  .description('authorize APIs and create config file')
  .action(function (env, options) {
    handleActionPromise(co(function *() {
      const gitLabProject = yield askGitLabToken();
      console.log();
      yield askGitLabPeers(gitLabProject);
      console.log();
      const trelloMeUser = yield askTelloToken();
      console.log();
      const boardId = yield askTrelloBoard(trelloMeUser);
      console.log();
      yield askTrelloLists(boardId);
    }));
  });
