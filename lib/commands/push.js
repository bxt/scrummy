const co = require('co');
const exec = require('co-exec');
const chalk = require('chalk');
const prompt = require('co-prompt');

const config = require('../config');
const MyTrello = require('../MyTrello');
const MyGitLab = require('../MyGitLab');
const { brace } = require('../utils');

const { handleActionPromise } = require('../utils');
const { moveCard, cardBranch, displayCheckItem } = require('../shared');

const ATTACHMENT_NAME = 'GitLab Merge Request';

const GITLAB_NL = `\r\n`;

const DESC_INTRO = '<!-- Details from the Trello card -->';
const DESC_OUTRO = `<!-- end Details -->${GITLAB_NL}`;

function* getCurrentCardFromBranch(t) {
  const branchName = yield exec('git rev-parse --abbrev-ref HEAD');
  const matches = branchName.match(/trello\/.*-(.*)/);
  if (!matches || !matches[1]) {
    throw new Error(`Not working on a trello branch (${branchName})`);
  }
  const shortLink = matches[1];
  const card = yield t.getCard(shortLink);
  console.log('Working on card %s with shortLink %s', card.name, card.shortLink);
  console.log(chalk.underline(card.url));
  return card;
}

function* pushCardBranch(card) {
  const branchName = cardBranch(card);
  const output = yield exec(`git push --porcelain origin ${branchName}`);
  const regex = `([=* ])\\s+refs/heads/${branchName}:refs/heads/${branchName}\\s+(.*)`;
  const result = new RegExp(regex).exec(output);
  if (!result) {
    console.log('no match', output);
  } else if (result[1] === '=' && result[2] === '[up to date]') {
    console.log(`Remote git was already up to date.`);
  } else if (result[1] === '*' && result[2] === '[new branch]') {
    console.log('Created the branch on the remote.');
  } else if (result[1] === ' ') {
    console.log(`Updated the branch on the remote. (${result[2]})`);
  } else {
    console.log('new params', result);
  }
}

function* askCardTickoff(t, card) {
  const checklist = yield t.getCardChecklist(card);
  if (checklist) {
    console.log(checklist.name);
    var allTicked = true;
    checklist.checkItems.forEach((item, i) => {
      displayCheckItem(item, brace(i) + ' ');
      const complete = item.state !== 'incomplete';
      allTicked = allTicked && complete;
    });
    if (!allTicked) {
      const itemsString = yield prompt('Which items to tick off? ');
      const itemStrings = itemsString.split(',');
      for (let i = 0; i < itemStrings.length; i++) {
        const item = checklist.checkItems[itemStrings[i].trim()];
        if (item) {
          console.log(`Checking ${item.name} with id ${item.id}...`);
          yield t.completeCardCheckItem(card, item);
        }
      }
    }
  }
}

function* getChecklistMarkdown(t, card) {
  let markdown = '';
  const checklist = yield t.getCardChecklist(card);
  if (checklist) {
    markdown += `### ${checklist.name}${GITLAB_NL}${GITLAB_NL}`;
    checklist.checkItems.forEach(item => {
      const complete = item.state !== 'incomplete';
      markdown += `- [${complete ? 'x' : ' '}] ${item.name}${GITLAB_NL}`;
    });
    markdown += `${GITLAB_NL}`;
  }
  return markdown;
}

function* getCardMarkdown(t, card) {
  let markdown = '';
  markdown += `${DESC_INTRO}${GITLAB_NL}${GITLAB_NL}`;
  markdown += `Trello: [${card.name}](${card.url})${GITLAB_NL}${GITLAB_NL}`;
  markdown += `${card.desc}${GITLAB_NL}${GITLAB_NL}`;
  markdown += yield getChecklistMarkdown(t, card);
  markdown += `${DESC_OUTRO}`;
  return markdown;
}

function* createOrSyncMR(t, card, gl) {
  const project = yield gl.getProject();
  let mergeRequest = yield gl.getMergeRequest(project);
  if (!mergeRequest) {
    console.log('Creating GitLab merge request...');
    mergeRequest = yield gl.addMergeRequest(project, card.name.replace(/^\(\d+\) /, ''));
  }

  const cardMarkdown = yield getCardMarkdown(t, card);

  let description = mergeRequest.description || '';
  const start = description.indexOf(DESC_INTRO);
  const end = description.indexOf(DESC_OUTRO);
  if (start === -1 || end === -1) {
    description = cardMarkdown + `\n` + description;
  } else {
    description = description.substring(0, start) + cardMarkdown + description.substring(end + DESC_OUTRO.length);
  }
  if (description !== mergeRequest.description) {
    console.log('Updating GitLab merge request description...');
    mergeRequest = yield gl.updateMergeRequest(mergeRequest, { description });
  }

  console.log(chalk.underline(mergeRequest.web_url));

  return mergeRequest;
}

function* putMRLinkIntoCardAttachment(t, card, mergeRequest) {
  if (yield t.ensureCardAttachment(card, ATTACHMENT_NAME, mergeRequest.web_url)) {
    console.log('Added MR-Link to the card.');
  }
}

function* askCardFinish(t, card) {
  const checklist = yield t.getCardChecklist(card);
  var allTicked = true;
  if (checklist) {
    checklist.checkItems.forEach((item, i) => {
      const complete = item.state !== 'incomplete';
      allTicked = allTicked && complete;
    });
  }
  if (allTicked) {
    const doFinish = yield prompt.confirm('The acceptance criteria are complete, do you want to finish the card?');
    return doFinish;
  }
}

function* assignReviewerToMR(gl, mergeRequest) {
  if (mergeRequest.assignee.id === (yield gl.getCurrentUser()).id) {
    const peerIDs = config.get('gitLabPeers');
    const peerID = peerIDs[Math.floor(Math.random() * peerIDs.length)];
    console.log('Re-Assigning the MR to peer #%d...', peerID);
    yield gl.updateMergeRequest(mergeRequest, { assignee_id: peerID });
  }
}

module.exports = program => program
  .command('push')
  .description('Save your changes on something')
  .action(function (env, options) {
    handleActionPromise(co(function *() {
      const t = MyTrello();
      const card = yield getCurrentCardFromBranch(t);
      yield pushCardBranch(card);
      yield askCardTickoff(t, card);
      const gl = yield MyGitLab();
      const mergeRequest = yield createOrSyncMR(t, card, gl);
      yield putMRLinkIntoCardAttachment(t, card, mergeRequest);
      const finished = yield askCardFinish(t, card);
      if (!finished) return;
      yield moveCard(t, card, 'review');
      yield assignReviewerToMR(gl, mergeRequest);
    }));
  });
