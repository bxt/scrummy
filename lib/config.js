const jsonfile = require('jsonfile');

class ConfigFile {
  constructor(file) {
    this.file = file;
  }

  get(prop, { defaultValue, ignoreNotFound = false } = {}) {
    const hasDefaultValue = defaultValue !== undefined;
    const config = this.getAll(ignoreNotFound || hasDefaultValue);
    const value = config[prop];
    if (value === undefined) {
      if (hasDefaultValue) {
        return defaultValue;
      }
      if (!ignoreNotFound) {
        throw new Error(`Config ${prop} not found, please run \`scrummy init\` to set it.`);
      }
    }
    return value;
  }

  set(prop, newValue) {
    const config = this.getAll(true);
    config[prop] = newValue;
    jsonfile.writeFileSync(this.file, config, {spaces: 2});
  }

  getAll(ignoreFileNotFound) {
    try {
      return jsonfile.readFileSync(this.file);
    } catch (err) {
      if (err.code === 'ENOENT') {
        if (ignoreFileNotFound) {
          return {};
        } else {
          throw new Error('Config file not found, please run `scrummy init` to create it.');
        }
      } else {
        throw err;
      }
    }
  }
}

const CONFIG_FILE = '.scrummy.json';
module.exports = new ConfigFile(CONFIG_FILE);
